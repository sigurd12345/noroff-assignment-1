// todo: Make balance, loan and cash private. Setter should update the display.

const BankAccount = new (function () {
    this.Balance = 0;
    this.Loan = 0;
    this.Lend = function () {
        const max = 2 * BankAccount.Balance;
        let amount = 0;

        // Get user input. If they write something other than a number, or if they enter too large a sum, prompt again.
        promptData = prompt("How big a loan would you like? (maximum " + max + ")", max);
        console.log(promptData)
        if (promptData == null) {
            // Stop the function if the user cancels the prompt
            return;
        }
        // Convert string to number
        amount = Number(promptData);
        // Check for NaN
        if (typeof (amount) == "number" && amount != NaN) {
            // Check size of the loan
            if (amount > max) {
                alert("The loan can't be greater than " + max + ".");
                return;
            }
            // Check for negative input
            if (amount < 0) {
                alert("The loan can't be less than 0")
                return;
            }

        } else {
            alert("Please enter a number.")
            return;
        }

        const Interest = 0.1;
        if (amount + this.Loan > 2 * this.Balance) {
            amount = (2 * this.Balance) - this.Loan;
        }
        this.Loan += Math.round(amount * (1 + Interest));
        this.Balance += amount;
    }
    this.PayBackLoan = function () {
        amount = Math.min(Person.Cash, this.Loan);
        this.Loan -= amount;
        Person.Cash -= amount;
    }
})();

const Person = new (function () {
    this.Cash = 0;
    this.Wage = 100;
    this.DepositCashToBank = function () {
        // If there is a loan, deduct 10% from the deposit and use it to pay the loan
        if (BankAccount.Loan > 0) {
            let deduction = Math.min(BankAccount.Loan, this.Cash * 0.1);
            this.Cash -= deduction;
            BankAccount.Loan -= deduction;
        }
        BankAccount.Balance += this.Cash;
        this.Cash = 0;
    }
    this.EarnWage = function () {
        Person.Cash += this.Wage;
    }
})();

const BalanceDisplay = document.getElementById("Balance");
const LoanDisplay = document.getElementById("Loan");
const WorkButton = document.getElementById("WorkButton");
const WalletBalanceDisplay = document.getElementById("WalletBalance");
const RepayButton = document.getElementById("RepayButton");
const LoanButton = document.getElementById("LoanButton");
// Updates displays of bank and wallet.
UpdateDisplays();
function UpdateDisplays() {
    BalanceDisplay.innerHTML = "Balance: " + BankAccount.Balance + " NOK";
    LoanDisplay.innerHTML = BankAccount.Loan > 0 ? "Outstanding loan: " + BankAccount.Loan + " NOK" : "";
    WorkButton.innerHTML = "Work (+" + Person.Wage + " NOK)";
    WalletBalanceDisplay.innerHTML = "Cash: " + Person.Cash + " NOK";
    RepayButton.style.visibility = (BankAccount.Loan > 0) ? "visible" : "hidden";
    LoanButton.style.visibility = (BankAccount.Balance > 0 && BankAccount.Loan === 0) ? "visible" : "hidden";

}
function OnAnyButtonPush() {
    UpdateDisplays();
}

// Subscribe events to ID'd buttons in the HTML
function SubscribeButton(ID, func) {
    document.getElementById(ID).addEventListener("click", () => { func(); OnAnyButtonPush(); });
}
SubscribeButton("WorkButton", () => Person.EarnWage());
SubscribeButton("LoanButton", () => BankAccount.Lend());
SubscribeButton("RepayButton", () => BankAccount.PayBackLoan());
SubscribeButton("CashToBank", () => Person.DepositCashToBank());

// Get computers and set up the shop
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(AllLaptopObjects => SetupShop(AllLaptopObjects));


let SelectedLaptop = undefined;
let Dropdown = document.getElementById("dropdown");
function SetupShop(AllLaptopObjects) {
    // Append to dropdown menu
    for (let i = 0; i < AllLaptopObjects.length; i++) {

        let option = Dropdown.appendChild(document.createElement("option"));
        option.innerHTML = AllLaptopObjects[i].title;
    }
    // Show the default selection
    SelectedLaptop = AllLaptopObjects[0];
    UpdateLaptopDisplay(AllLaptopObjects[0]);
    // When dropdown changes, get the selected laptop and update the graphic.
    Dropdown.addEventListener("change", function (event) {
        for (let i = 0; i < AllLaptopObjects.length; i++) {
            if (Dropdown.value == AllLaptopObjects[i].title) {
                SelectedLaptop = AllLaptopObjects[i];
                UpdateLaptopDisplay(AllLaptopObjects[i]);
                return;
            }
        }
    })
    // Now that the shop is set up correctly, we can subscribe the "buyLaptop" event to the "buy" button.
    SubscribeButton("buy laptop", BuyTheSelectedLaptop);
}

let PrevLaptop = undefined;
// This will update the laptop name, specs, price, and picture when the user selects a new laptop in the dropdown.
function UpdateLaptopDisplay(LaptopObject) {
    if (LaptopObject == PrevLaptop) {
        //  Return if there is no change.
        return;
    }

    document.getElementById("laptop name").innerHTML = LaptopObject.title;
    document.getElementById("laptop description").innerHTML = LaptopObject.description;
    document.getElementById("laptop specs").innerHTML = LaptopObject.specs.join("<br>");
    let price = LaptopObject.price + " NOK";
    document.getElementById("price").innerHTML = price;
    let url = "https://noroff-komputer-store-api.herokuapp.com/" + LaptopObject.image;
    if (LaptopObject.title == "The Visor") {
        // Repair broken url
        url = "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png";
    }

    document.getElementById("laptop image").innerHTML = '<img src = "' + url + '">';

    PrevLaptop = LaptopObject;
}

function BuyTheSelectedLaptop() {
    if (SelectedLaptop == undefined) {
        return;
    }

    let price = SelectedLaptop.price - 0;
    if (price > BankAccount.Balance) {
        alert("That laptop too expensive. You have " + BankAccount.Balance + " NOK in your bank account.");
        return;
    }
    BankAccount.Balance -= price;
    alert("You are now the proud owner of a " + SelectedLaptop.title + "! " + price + " NOK has been subtracted from your bank balance.");
}